﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Ecommerce.Domain.Enums;
using Ecommerce.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Domain
{
    public class ApplicationDbContextInitializer : IApplicationDbContextInitializer
    {
        private readonly ApplicationDbContext _context;

        public ApplicationDbContextInitializer(ApplicationDbContext context)
        {
            _context = context;
        }

        public bool EnsureCreated()
        {
            return _context.Database.EnsureCreated();
        }

        public void Migrate()
        {
            _context.Database.Migrate();
        }

        public async Task Seed()
        {
            await SeedCategory(_context);
            await SeedSupplier(_context);

            await SeedManufature(_context);
            await SeedProduct(_context);

            await SeedProductInCategory(_context);

            await SeedCustomer(_context);

            await SeedOrder(_context);
            await SeedOrderDetail(_context);

            await SeedUser(_context);
            await SeedUserProfile(_context);

        }

        #region SeedData
        private async Task SeedSupplier(ApplicationDbContext context)
        {
            if (!context.Suppliers.Any())
            {
                context.Suppliers.Add(new Supplier { 
                    Name = "Tiki",
                    CodeName = "Tiki",
                    Phone = "0968684457"
                });

                await context.SaveChangesAsync();
            }
        }

        private async Task SeedProduct(ApplicationDbContext context)
        {
            var manufature = await context.Manufactures.FirstOrDefaultAsync(x => x.Name == "LG");
            var supplier = await context.Suppliers.FirstOrDefaultAsync(x => x.Name == "Tiki");

            if (!context.Products.Any())
            {
                context.Products.Add(new Product { 
                    SupplierId = supplier.Id,
                    ManufactureId = manufature.Id,
                    Name = "Samsung s10",
                    Sku = "Sgdk323",
                    CommonStatus = Enums.Status.Active,
                    Views = 100,
                    ProductStatus = ProductStatusEnum.New,
                    ShortDescription = "Mô tả ngắn",
                    Description = "Mô tả chi tiết",
                    PublicationDate = DateTime.Now,
                    Code = "CNMSC11X",
                    Price = 1500000,
                    Quantity = 50,
                    Width = 5,
                    Height = 10,
                    CreatedDate = DateTime.Now,
                    Keyword = "samsung",
                    Weight = 4,
                    PercentDiscount = 10
                });

                context.Products.Add(new Product
                {
                    SupplierId = supplier.Id,
                    ManufactureId = manufature.Id,
                    Name = "Lg s10",
                    Sku = "czxz",
                    CommonStatus = Enums.Status.Active,
                    Views = 100,
                    ProductStatus = ProductStatusEnum.New,
                    ShortDescription = "Mô tả ngắn",
                    Description = "Mô tả chi tiết",
                    PublicationDate = DateTime.Now,
                    Code = "NDKCM",
                    Price = 2000000,
                    Quantity = 30,
                    Width = 5,
                    Height = 2,
                    CreatedDate = DateTime.Now,
                    Keyword = "samsung",
                    Weight = 4,
                    PercentDiscount = 10
                });

                context.Products.Add(new Product
                {
                    SupplierId = supplier.Id,
                    ManufactureId = manufature.Id,
                    Name = "Sony",
                    Sku = "czxz",
                    CommonStatus = Enums.Status.Active,
                    Views = 100,
                    ProductStatus = ProductStatusEnum.New,
                    ShortDescription = "Mô tả ngắn",
                    Description = "Mô tả chi tiết",
                    PublicationDate = DateTime.Now,
                    Code = "NDKCM",
                    Price = 1700000,
                    Quantity = 30,
                    Width = 5,
                    Height = 2,
                    CreatedDate = DateTime.Now,
                    Keyword = "samsung",
                    Weight = 4,
                    PercentDiscount = 10
                });

                context.Products.Add(new Product
                {
                    SupplierId = supplier.Id,
                    ManufactureId = manufature.Id,
                    Name = "Oppo",
                    Sku = "gfgvf",
                    CommonStatus = Enums.Status.Active,
                    Views = 50,
                    ProductStatus = ProductStatusEnum.New,
                    ShortDescription = "Mô tả ngắn",
                    Description = "Mô tả chi tiết",
                    PublicationDate = DateTime.Now,
                    Code = "MSCCS",
                    Price = 1300000,
                    Quantity = 30,
                    Width = 5,
                    Height = 2,
                    CreatedDate = DateTime.Now,
                    Keyword = "samsung",
                    Weight = 4,
                    PercentDiscount = 10,
                });

                await context.SaveChangesAsync();
            }
        }

        private async Task SeedCategory(ApplicationDbContext context)
        {
            if (!context.Categories.Any())
            {
                context.Categories.Add(new Category { 
                    Name = "Điện thoại - Máy tính bảng",
                    Description = "Danh mục điện thoại, máy tính bảng",
                    ParentId = null,
                    IsDisplayHomePage = true,
                    CreatedDate = DateTime.Now,
                    MetaTitle = "dien-thoai",
                    CommonStatus = Status.Active
                });

                context.Categories.Add(new Category
                {
                    Name = "Điện lạnh",
                    Description = "Danh mục điện thoại, máy tính bảng",
                    ParentId = null,
                    IsDisplayHomePage = true,
                    CreatedDate = DateTime.Now,
                    MetaTitle = "dien-thoai",
                    CommonStatus = Status.Active
                });

                await context.SaveChangesAsync();

                var category = await context.Categories.FirstOrDefaultAsync(x => x.Name == "Điện thoại - Máy tính bảng");

                context.Categories.Add(new Category
                {
                    Name = "Iphone 12",
                    Description = "Danh mục điện thoại, máy tính bảng",
                    ParentId = category.Id,
                    IsDisplayHomePage = true,
                    CreatedDate = DateTime.Now,
                    MetaTitle = "dien-thoai",
                    CommonStatus = Status.Active
                });

                context.Categories.Add(new Category
                {
                    Name = "Iphone 11",
                    Description = "Danh mục điện thoại, máy tính bảng",
                    ParentId = category.Id,
                    IsDisplayHomePage = true,
                    CreatedDate = DateTime.Now,
                    MetaTitle = "dien-thoai",
                    CommonStatus = Status.Active
                });

                await context.SaveChangesAsync();
            }
        }

        private async Task SeedOrderDetail(ApplicationDbContext context)
        {
            if (!context.OrderDetails.Any())
            {
                var order = await context.Orders.FirstOrDefaultAsync(x => x.Address == "Số 20, ngõ 192 Lê Trọng Tấn");
                var product = await context.Products.FirstOrDefaultAsync(x => x.Name == "Samsung s10");

                context.OrderDetails.Add(new OrderDetail { 
                   Quantity = 1,
                   Price = 11000000,
                   Voucher = null,
                   OrderId = order.Id,
                   ProductId = product.Id
                });

                context.OrderDetails.Add(new OrderDetail
                {
                    Quantity = 1,
                    Price = 2000000,
                    Voucher = null,
                    OrderId = order.Id,
                    ProductId = product.Id
                });
                await context.SaveChangesAsync();
            }
        }

        private async Task SeedCustomer(ApplicationDbContext context)
        {
            if (!context.Customers.Any())
            {
                context.Customers.Add(new Customer { 
                    Name = "Nguyễn Bá Tình",
                    Address = "Ngô Nội, Trung Nghĩa, Yên Phong, Bắc Ninh",
                    Phone = "0947282765",
                    CustomerAddresses = null,
                    Email = "batinhypbn@gmail.com"
                });

                await context.SaveChangesAsync();
            }
        }

        private async Task SeedOrder(ApplicationDbContext context)
        {
            if (!context.Orders.Any())
            {
                var customer = await context.Customers.FirstOrDefaultAsync(x => x.Email.Equals("batinhypbn@gmail.com"));

                context.Orders.Add(new Order { 
                    PaymentType = Payment.Online,
                    Phone = "0968684457",
                    OrderNumber = 1,
                    OrderStatus = OrderStatusEnum.Shipped,
                    VoucherCode = null,
                    FeeMount = 0,
                    Discount = 0,
                    Address = "Số 20, ngõ 192 Lê Trọng Tấn",
                    Province = "Bắc Ninh",
                    Districts = "Yên Phong",
                    Ward = "Trung Nghĩa",
                    TotalPrice = 13000000,
                    CustomerId = customer.Id
                });

                await context.SaveChangesAsync();
            }
        }

        private async Task SeedUserProfile(ApplicationDbContext context)
        {
            if (!context.UserProfiles.Any())
            {
                var user1 = await context.Users.FirstOrDefaultAsync(x => x.Username == "batinh0612");
                var user2 = await context.Users.FirstOrDefaultAsync(x => x.Username == "badung0411");
                var user3 = await context.Users.FirstOrDefaultAsync(x => x.Username == "thaihoc");
                var user4 = await context.Users.FirstOrDefaultAsync(x => x.Username == "minhhieu");
                var user5 = await context.Users.FirstOrDefaultAsync(x => x.Username == "trinhha");
                var user6 = await context.Users.FirstOrDefaultAsync(x => x.Username == "hoangnam");

                context.UserProfiles.Add(new UserProfile { 
                    Address = "Yên Phong - Bắc Ninh",
                    Avatar = "fdsdjsnf.jpg",
                    Email = "batinhypbn@gmail.com",
                    Phone = "0968684457",
                    UserId = user1.Id
                });

                context.UserProfiles.Add(new UserProfile
                {
                    Address = "Yên Phong - Bắc Ninh",
                    Avatar = "fvcxvcx.jpg",
                    Email = "badung0411@gmail.com",
                    Phone = "0397779413",
                    UserId = user2.Id
                });

                context.UserProfiles.Add(new UserProfile
                {
                    Address = "Yên Phong - Bắc Ninh",
                    Avatar = "fdsdjsnf.jpg",
                    Email = "batinhypbn@gmail.com",
                    Phone = "0968684457",
                    UserId = user3.Id
                });

                context.UserProfiles.Add(new UserProfile
                {
                    Address = "Yên Phong - Bắc Ninh",
                    Avatar = "fvcxvcx.jpg",
                    Email = "badung0411@gmail.com",
                    Phone = "0397779413",
                    UserId = user4.Id
                });

                context.UserProfiles.Add(new UserProfile
                {
                    Address = "Yên Phong - Bắc Ninh",
                    Avatar = "fdsdjsnf.jpg",
                    Email = "batinhypbn@gmail.com",
                    Phone = "0968684457",
                    UserId = user5.Id
                });

                context.UserProfiles.Add(new UserProfile
                {
                    Address = "Yên Phong - Bắc Ninh",
                    Avatar = "fvcxvcx.jpg",
                    Email = "badung0411@gmail.com",
                    Phone = "0397779413",
                    UserId = user6.Id
                });

                await context.SaveChangesAsync();
            }
        }

        private async Task SeedUser(ApplicationDbContext context) {
            if (!context.Users.Any())
            {
                context.Add(new User { 
                    FirstName = "Nguyễn Bá",
                    LastName = "Tình",
                    Username = "batinh0612",
                    PasswordHash = null,
                    PasswordSalt = null,
                    RoleId = null
                });

                context.Add(new User
                {
                    FirstName = "Nguyễn Bá",
                    LastName = "Dũng",
                    Username = "badung0411",
                    PasswordHash = null,
                    PasswordSalt = null,
                    RoleId = null
                });

                context.Add(new User
                {
                    FirstName = "Nguyễn Thái",
                    LastName = "Học",
                    Username = "thaihoc",
                    PasswordHash = null,
                    PasswordSalt = null,
                    RoleId = null
                });

                context.Add(new User
                {
                    FirstName = "Nguyễn Minh",
                    LastName = "Hiếu",
                    Username = "minhhieu",
                    PasswordHash = null,
                    PasswordSalt = null,
                    RoleId = null
                });

                context.Add(new User
                {
                    FirstName = "Nguyễn Trịnh",
                    LastName = "Hà",
                    Username = "trinhha",
                    PasswordHash = null,
                    PasswordSalt = null,
                    RoleId = null
                });

                context.Add(new User
                {
                    FirstName = "Lê Hoàng",
                    LastName = "Nam",
                    Username = "hoangnam",
                    PasswordHash = null,
                    PasswordSalt = null,
                    RoleId = null
                });

                await context.SaveChangesAsync();
            }
        }

        private async Task SeedManufature(ApplicationDbContext context)
        {
            if (!context.Manufactures.Any())
            {
                context.Add(new Manufacture
                {
                    Name = "LG",
                    Phone = "0984236412",
                    Email = "lg@gmail.com",
                    CodeName = "NDS4105D",
                    Fax = "cjdasdad",
                    CommonStatus = Status.Active,
                });

                context.Add(new User
                {
                    FirstName = "Nguyễn Bá",
                    LastName = "Dũng",
                    Username = "badung0411",
                    PasswordHash = null,
                    PasswordSalt = null,
                    RoleId = null
                });

                await context.SaveChangesAsync();
            }
        }

        private async Task SeedProductInCategory(ApplicationDbContext context)
        {
            if (!context.ProductInCategories.Any())
            {
                var product1 = await context.Products.FirstOrDefaultAsync(x => x.Name == "Samsung s10");
                var product2 = await context.Products.FirstOrDefaultAsync(x => x.Name == "Lg s10");
                var product3 = await context.Products.FirstOrDefaultAsync(x => x.Name == "Sony");
                var product4 = await context.Products.FirstOrDefaultAsync(x => x.Name == "Oppo");

                var category1 = await context.Categories.FirstOrDefaultAsync(x =>  x.Name == "Điện thoại - Máy tính bảng");
                var category2 = await context.Categories.FirstOrDefaultAsync(x => x.Name == "Điện lạnh");
                var category3 = await context.Categories.FirstOrDefaultAsync(x => x.Name == "Iphone 12");
                var category4 = await context.Categories.FirstOrDefaultAsync(x => x.Name == "Iphone 11");

                context.ProductInCategories.Add(new ProductInCategory() { 
                    ProductId = product1.Id,
                    CategoryId = category1.Id
                });

                context.ProductInCategories.Add(new ProductInCategory()
                {
                    ProductId = product2.Id,
                    CategoryId = category2.Id
                });

                context.ProductInCategories.Add(new ProductInCategory()
                {
                    ProductId = product3.Id,
                    CategoryId = category3.Id
                });

                context.ProductInCategories.Add(new ProductInCategory()
                {
                    ProductId = product4.Id,
                    CategoryId = category4.Id
                });
            }
        }
        #endregion
    }
}