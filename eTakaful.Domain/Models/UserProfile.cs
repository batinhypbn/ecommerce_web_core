﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecommerce.Domain.Models
{
    public class UserProfile : BaseModel
    {
        #region Property
        public string Address { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Avatar { get; set; }
        #endregion

        #region Relationship
        public Guid UserId { get; set; }
        public virtual User User { get; set; }
        #endregion
    }
}
