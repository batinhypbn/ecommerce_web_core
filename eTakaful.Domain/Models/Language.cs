﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Ecommerce.Domain.Models
{
    public class Language : BaseModel
    {
        #region Property
        [Required, MaxLength(20)]
        public string Name { get; set; }
        public bool IsDefault { get; set; }
        #endregion
    }
}
