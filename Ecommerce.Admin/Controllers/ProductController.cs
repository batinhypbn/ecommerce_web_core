﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ecommerce.Repository.Interfaces;
using Ecommerce.Service.Interface;
using EcommerceCommon.Infrastructure.Dto.Product;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Ecommerce.Admin.Controllers
{
    public class ProductController : Controller
    {
        //private readonly IProductRepository _productRepository;
        //private readonly ICategoryRepository _categoryRepository;
        //private readonly ISupplierRepository _supplierRepository;
        //private readonly IManufactureRepository _manufactureRepository;

        //public ProductController(IProductRepository productRepository,
        //    ICategoryRepository categoryRepository,
        //    ISupplierRepository supplierRepository,
        //    IManufactureRepository manufactureRepository)
        //{
        //    _productRepository = productRepository;
        //    _categoryRepository = categoryRepository;
        //    _supplierRepository = supplierRepository;
        //    _manufactureRepository = manufactureRepository;
        //}

        private readonly IProductSevice _productSevice;
        private readonly ICategoryService _categoryService;
        private readonly ISupplierService _supplierService;
        private readonly IManufactureService _manufactureService;

        public ProductController(IProductSevice productSevice, 
            ICategoryService categoryService,
            ISupplierService supplierService,
            IManufactureService manufactureService)
        {
            _productSevice = productSevice;
            _categoryService = categoryService;
            _supplierService = supplierService;
            _manufactureService = manufactureService;
        }

        [HttpGet]
        public async Task<IActionResult> Index(Guid? categoryId)
        {
            ViewBag.Success = TempData["result"];

            var categories = await _categoryService.GetAllCategories();

            ViewBag.Categories = categories.Select(x => new SelectListItem()
            {
                Text = x.Name,
                Value = x.Id.ToString(),
                Selected = categoryId.HasValue && categoryId == x.Id
            });

            var products = await _productSevice.GetAllProducts(categoryId);
            return View(products);
        }

        [HttpGet]
        public async Task<IActionResult> Details(Guid id)
        {
            var product = await _productSevice.GetProductById(id);
            return View(product);
        }

        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var suppliers = await _supplierService.GetAll();

            ViewBag.Suppliers = suppliers.Select(x => new SelectListItem() { 
                Text = x.Name,
                Value = x.Id.ToString()
            });

            var manufactures = await _manufactureService.GetAll();

            ViewBag.Manufactures = manufactures.Select(x => new SelectListItem() { 
                Text = x.Name,
                Value = x.Id.ToString()
            });

            var categories = await _categoryService.GetAllCategories();

            ViewBag.Categories = categories.Select(x => new SelectListItem() { 
                Text = x.Name,
                Value = x.Id.ToString()
            });

            return View();
        }

        [HttpPost]
        [Consumes("multipart/form-data")]//cho phep nhan kieu du lieu truyen len la form-data
        public async Task<IActionResult> Create(ProductCreateDto dto)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var result = await _productSevice.Create(dto);

            if (result)
            {
                TempData["result"] = "Thêm sản phẩm thành công";
                return RedirectToAction("Index");
            }

            ModelState.AddModelError("", "Thêm sản phẩm thất bại");

            return View(dto);
        }
    }
}
