﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ecommerce.Service.Interface;
using EcommerceCommon.Infrastructure.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace Ecommerce.Admin.Controllers
{
    public class DashboardController : Controller
    {
        private readonly IDashboardService _dashboardService;

        public DashboardController(IDashboardService dashboardService)
        {
            _dashboardService = dashboardService;
        }

        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            var models = new DashboardViewModel
            {
                TotalOrder = await _dashboardService.GetTotalOrder(),
                NewUserRegisters = await _dashboardService.GetUserRegisters(),
                NewProduct = await _dashboardService.GetNewProduct(),
                MostViewProducts = await _dashboardService.GetProductView(),
                NewOrders = await _dashboardService.GetNewOrder()
            };
            return View(models);
        }

        /// <summary>
        /// New order
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> ListNewOrder()
        {
            var newOrder = await _dashboardService.GetNewOrder();
            return View(newOrder);
        }

        /// <summary>
        /// New product
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> NewProduct()
        {
            var newProduct = await _dashboardService.GetNewProduct();
            return View(newProduct);
        }

        /// <summary>
        /// New user
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> NewUser()
        {
            var users = await _dashboardService.GetUserRegisters();
            return View(users);
        }
    }
}
