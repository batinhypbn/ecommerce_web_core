using AutoMapper;
using Ecommerce.Domain.Models;
using Ecommerce.Service.Dto;
using Ecommerce.Service.ViewModels;
using EcommerceCommon.Infrastructure.ViewModel.Product;

namespace Ecommerce.Core.ViewModels
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            MappingEntityToViewModel();
            MappingViewModelToEntity();
        }

        private void MappingEntityToViewModel()
        {
            // case get data
            CreateMap<Category, CategoryVM>();
            //.ForMember(x => x.Description, options => options.MapFrom(x => x.MetaTitle))
            CreateMap<Product, ProductViewModel>();
            CreateMap<User, UserDto>();
        }

        private void MappingViewModelToEntity()
        {
            // case insert or update
            CreateMap<CategoryDto, Category>();
            CreateMap<UserDto, User>();
            CreateMap<RoleDto, Role>();
        }
    }
}