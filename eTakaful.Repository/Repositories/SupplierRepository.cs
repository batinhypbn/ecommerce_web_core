﻿using Ecommerce.Domain;
using Ecommerce.Domain.Models;
using Ecommerce.Repository.Interfaces;
using EcommerceCommon.Infrastructure.Dto.Supplier;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Repository
{
    public class SupplierRepository : ISupplierRepository
    {
        private readonly ApplicationDbContext DbContext;
        public SupplierRepository(ApplicationDbContext dbContext) {
            DbContext = dbContext;
        }

        /// <summary>
        /// Get all
        /// </summary>
        /// <returns></returns>
        public async Task<List<SupplierListItem>> GetAll()
        {
            var suppliers = DbContext.Suppliers.Select(x => new SupplierListItem() { 
                Id = x.Id,
                Name = x.Name
            });

            return await suppliers.ToListAsync();
        }
    }
}
