﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ecommerce.Domain;
using Ecommerce.Domain.Models;
using Ecommerce.Repository.Interfaces;
using EcommerceCommon.Infrastructure.Dto.Category;
using EcommerceCommon.Infrastructure.ViewModel.Category;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Repository
{
    public class CategoryRepository : BaseRepository<Category>, ICategoryRepository
    {

        public CategoryRepository(ApplicationDbContext dbContext) : base(dbContext)
        {
        }

        /// <summary>
        /// Get category parrent
        /// </summary>
        /// <returns></returns>
        public async Task<ICollection<Category>> GetCategoryParrent()
        {
            return await DbContext.Categories.Where(c => c.ParentId == null).ToListAsync();
        }

        /// <summary>
        /// Get by id async
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override async Task<Category> GetByIdAsync(Guid id)
        {
            return await DbContext.Set<Category>().FindAsync(id);
        }

        /// <summary>
        /// Get all categories
        /// </summary>
        /// <returns></returns>
        public async Task<List<CategoryListItem>> GetAllCategories()
        {
            var categories = DbContext.Categories.Select(x => new CategoryListItem() { 
                Id = x.Id,
                Name = x.Name
            });

            return await categories.ToListAsync();
        }

        /// <summary>
        /// Get list categories
        /// </summary>
        /// <returns></returns>
        public async Task<List<CategoryViewModel>> GetListCategories()
        {
            var categories = await DbContext.Categories.Select(x => new CategoryViewModel()
            {
                Id = x.Id,
                Description = x.Description,
                IsDisplayHomePage = x.IsDisplayHomePage,
                MetaTitle = x.MetaTitle,
                Name = x.Name,
                ParentId = x.ParentId,
                CreatedDate = x.CreatedDate
            }).ToListAsync();

            return categories;
        }

        /// <summary>
        /// Get category by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<CategoryViewModel> GetCategoryById(Guid id)
        {
            var query = await DbContext.Categories.FindAsync(id);

            Category parentName = new Category();
            if (query.ParentId != null)
            {
                 parentName = DbContext.Categories.Find(query.ParentId);
            }

            var category = new CategoryViewModel()
            {
                CreatedDate = query.CreatedDate,
                Description = query.Description,
                Id = query.Id,
                IsDisplayHomePage = query.IsDisplayHomePage,
                MetaTitle = query.MetaTitle,
                Name = query.Name,
                ParentId = query.ParentId,
                CommonStatus = query.CommonStatus,
                CategoryName = string.IsNullOrEmpty(parentName.Name) ? "Không có" : parentName.Name
            };

            return category;
        }

        /// <summary>
        /// Create category
        /// </summary>
        /// <returns></returns>
        public async Task<bool> Create(CategoryCreateDto dto)
        {
            var category = new Category()
            {
                Description = dto.Description,
                IsDisplayHomePage = dto.IsDisplayHomePage,
                MetaTitle = dto.MetaTitle,
                Name = dto.Name,
                ParentId = dto.ParentId
            };

            DbContext.Categories.Add(category);
            await DbContext.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Update
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<int> Update(CategoryUpdateDto dto)
        {
            var category = await DbContext.Categories.FindAsync(dto.Id);

            category.Description = dto.Description;
            category.Name = dto.Name;
            category.ParentId = dto.ParentId;
            category.UpdatedDate = DateTime.Now;

            DbContext.Categories.Update(category);

            return await DbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Change status
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> ChangeStatus(Guid id)
        {
            var category = await DbContext.Categories.FindAsync(id);

            category.IsDisplayHomePage = !category.IsDisplayHomePage;
            await DbContext.SaveChangesAsync();

            return category.IsDisplayHomePage;
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> Delete(Guid id)
        {
            var category = await DbContext.Categories.FindAsync(id);

            if (category != null)
            {
                DbContext.Categories.Remove(category);
                await DbContext.SaveChangesAsync();
                return true;
            }

            return false;
        }
    }
}
