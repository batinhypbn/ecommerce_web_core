﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Ecommerce.Domain;
using Ecommerce.Domain.Enums;
using Ecommerce.Domain.Models;
using Ecommerce.Repository.Common;
using Ecommerce.Repository.Interfaces;
using EcommerceCommon.Infrastructure.Dto.Product;
using EcommerceCommon.Infrastructure.ViewModel;
using EcommerceCommon.Infrastructure.ViewModel.Product;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace Ecommerce.Repository
{
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        private readonly IStorageRepository _storageRepository;

        public ProductRepository(ApplicationDbContext dbContext, IStorageRepository storageRepository) : base(dbContext)
        {
            _storageRepository = storageRepository;
        }

        /// <summary>
        /// Create
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<bool> Create(ProductCreateDto dto)
        {
            var product = new Product()
            {
                Code = dto.Code,
                Description = dto.Description,
                Keyword = dto.Keyword,
                Name = dto.Name,
                PercentDiscount = dto.PercentDiscount,
                Width = dto.Width,
                Height = dto.Height,
                Weight = dto.Weight,
                Views = 0,
                Sku = dto.Sku,
                ShortDescription = dto.ShortDescription,
                Quantity = dto.Quantity,
                PublicationDate = dto.PublicationDate,
                ExpirationDate = dto.ExpirationDate,
                Price = dto.Price,
                CommonStatus = Status.Active,
                ProductStatus = ProductStatusEnum.New,
                SupplierId = dto.SupplierId,
                ManufactureId = dto.ManufactureId,
                ProductInCategories = new List<ProductInCategory>()
                {
                    new ProductInCategory()
                    {
                        CategoryId = dto.CategoryId
                    }
                }
            };

            if (dto.ThumnailImage != null)
            {
                product.ProductImages = new List<ProductImage>()
                {
                    new ProductImage()
                    {
                        ImageLink = await this.SaveFile(dto.ThumnailImage),
                        MainImage = true
                    }
                };
            }

            DbContext.Products.Add(product);
            await DbContext.SaveChangesAsync();

            return true;
        }

        /// <summary>
        /// Save file
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private async Task<string> SaveFile(IFormFile file)
        {
            var originalFileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
            var fileName = $"{Guid.NewGuid()}{Path.GetExtension(originalFileName)}";
            await _storageRepository.SaveFileAsync(file.OpenReadStream(), fileName);
            return fileName;
        }

        /// <summary>
        /// Get all products
        /// </summary>
        /// <returns></returns>
        public async Task<List<ProductViewModel>> GetAllProducts(Guid? categoryId)
        {
            var query = await (from p in DbContext.Products
                        join pic in DbContext.ProductInCategories on p.Id equals pic.ProductId
                        into picc
                        from pic in picc.DefaultIfEmpty()
                        join c in DbContext.Categories on pic.CategoryId equals c.Id into cc
                        from c in cc.DefaultIfEmpty()
                        join s in DbContext.Suppliers on p.SupplierId equals s.Id
                        join m in DbContext.Manufactures on p.ManufactureId equals m.Id
                        select new ProductViewModel{ 
                            Id = p.Id,
                            Name = p.Name,
                            Code = p.Code,
                            Description = p.Description,
                            ExpirationDate = p.ExpirationDate,
                            Price = p.Price,
                            Quantity = p.Quantity,
                            Views = p.Views,
                            ShortDescription = p.ShortDescription,
                            PublicationDate = p.PublicationDate,
                            PercentDiscount = p.PercentDiscount,
                            ProductStatus = p.ProductStatus,
                            Sku = p.Sku,
                            Keyword = p.Keyword,
                            CategoryId = pic.CategoryId
                        }).ToListAsync();

            var reuslt = query;

            if (categoryId != null)
            {
                query = query.Where(x => x.CategoryId == categoryId).ToList();
            }

            return query;
        }

        /// <summary>
        /// Get new product
        /// </summary>
        /// <returns></returns>
        public async Task<List<ProductViewModel>> GetNewProduct()
        {
            DateTime startDateTime;
            DateTime endDateTime;

            startDateTime = DateTime.Today;//Today at 00:00:00
            endDateTime = DateTime.Today.AddDays(1).AddTicks(-1);//Today at 23:59:59

            var query = from p in DbContext.Products.Where(x => x.CreatedDate >= startDateTime && x.CreatedDate <= endDateTime)
                              join s in DbContext.Suppliers on p.SupplierId equals s.Id
                              select new ProductViewModel
                              {
                                  Code = p.Code,
                                  Name = p.Name,
                                  Quantity = p.Quantity,
                                  Price = p.Price,
                                  SupplierName = s.Name,
                                  ProductStatus = p.ProductStatus,
                                  Views = p.Views,
                                  PublicationDate = p.PublicationDate
                              };
            return await query.ToListAsync();
        }

        /// <summary>
        /// GetProductByCategoryIdAndOrderByView
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public async Task<List<Product>> GetProductByCategoryIdAndOrderByView(Guid categoryId)
        {

            var products = await DbContext.Products.Where(c => c.Views >= 100).Take(100)
                .ToListAsync();
            return products;
        }

        /// <summary>
        /// Get product by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ProductViewModel> GetProductById(Guid id)
        {
            var query = from p in DbContext.Products
                        join pic in DbContext.ProductInCategories on p.Id equals pic.ProductId into picc
                        from pic in picc.DefaultIfEmpty()
                        join s in DbContext.Suppliers on p.SupplierId equals s.Id
                        join m in DbContext.Manufactures on p.ManufactureId equals m.Id
                        where p.Id == id
                        select new { p, pic, s, m };

            var productViewModel = await query.Select(x => new ProductViewModel() {
                Code = x.p.Code,
                Description = x.p.Description,
                ExpirationDate = x.p.ExpirationDate,
                Height = x.p.Height,
                Name = x.p.Name,
                PercentDiscount = x.p.PercentDiscount,
                Price = x.p.Price,
                Quantity = x.p.Quantity,
                Keyword = x.p.Keyword,
                Id = x.p.Id,
                ProductStatus = x.p.ProductStatus,
                PublicationDate = x.p.PublicationDate,
                ShortDescription = x.p.ShortDescription,
                Sku = x.p.Sku,
                Views = x.p.Views,
                Weight = x.p.Weight,
                Width = x.p.Width,
                ManufactureName = x.m.Name,
                SupplierName =  x.s.Name
            }).FirstOrDefaultAsync();

            return productViewModel;
        }

        /// <summary>
        /// Get View Product
        /// </summary>
        /// <returns></returns>
        public async Task<List<MostViewProductViewModel>> GetProductView()
        {
            var product = await (
                    from p in DbContext.Products.Where(x => x.IsDeleted == false).OrderByDescending(x => x.Views).Take(5)
                    join pic in DbContext.ProductInCategories on p.Id equals pic.ProductId
                    join c in DbContext.Categories on pic.CategoryId equals c.Id
                    join s in DbContext.Suppliers on p.SupplierId equals s.Id
                    select new MostViewProductViewModel
                    {
                        ProductId = p.Id,
                        CategoryName = c.Name,
                        Price = p.Price,
                        SupplierName = s.Name,
                        Name = p.Name,
                        Views = p.Views,
                        Code = p.Code,
                        Quantity = p.Quantity
                    }
                ).ToListAsync();
            return product;
        }

        /// <summary>
        /// GrowUpViewByProductId
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public async Task<bool> GrowUpViewByProductId(Guid productId)
        {
            // get product by id
            var product = await GetByIdAsync(productId);
            if (product != null)
            {
                product.Views = product.Views + 1;
                await UpdateAsync(product);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Get product by categoryId
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //public async Task<List<ProductViewModel>> GetProductByCategoryId(Guid id)
        //{
        //    var 
        //}
    }
}
