﻿using Ecommerce.Domain.Models;
using EcommerceCommon.Infrastructure.Dto.Supplier;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ecommerce.Repository.Interfaces
{
    public interface ISupplierRepository
    {
        /// <summary>
        /// Get all
        /// </summary>
        /// <returns></returns>
        Task<List<SupplierListItem>> GetAll();
    }
}
